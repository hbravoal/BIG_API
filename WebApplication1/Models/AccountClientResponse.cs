﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{

    public class AccountClientResponse
    {
        public bool Result { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
        
    }
}