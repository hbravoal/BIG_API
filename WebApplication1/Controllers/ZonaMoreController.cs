﻿using WebApplication1.Models;
using DataContext;


using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Configuration;


namespace WebApplication1.Controllers
{
    [RoutePrefix("api/ZonaMore")]
    [AllowAnonymous]
    public class ZonaMController : ApiController
    {
        [HttpPost]
        [Route("GetConsumerByDNI")]
        public  bool Get(JObject form)
        {
            dbLoyaltyNetworkSuraEntities db = new dbLoyaltyNetworkSuraEntities();
          
                var number = string.Empty; //Numero Identidad
                var typeConvert = string.Empty;     //Cast(Tipo Identidad)
                int type;
                dynamic jsonObject = form;
                int programID=Convert.ToInt32(ConfigurationManager.AppSettings["ProgramID"]);
                try
                {
                    number = jsonObject.IdentificationNumber.Value;
                    typeConvert = jsonObject.IdentificationType.Value;
                    type = Convert.ToInt32(typeConvert);
                    var user = (from u in db.Consumers 
                                join acc in db.Accounts on u.Id equals acc.CustomerId
                                where ( u.IdentificationTypeId == type && u.Identification == number && acc.ProgramId==programID )
                                select u).FirstOrDefault();

                    return user == null ? false : true;
                   
                }
                    //DataBase Validation
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }

                
            }
            
        


       

        
    }
}
