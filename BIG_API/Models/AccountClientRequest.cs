﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BIG_API.Models
{
    public class AccountClientRequest
    {
        public int ProgramId { get; set; }

        public int IdentificationTypeId { get; set; }

        public string IdentificationNumber { get; set; }

    }
}