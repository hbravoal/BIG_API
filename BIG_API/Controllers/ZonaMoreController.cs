﻿using BIG_API.Models;
using DataContext;


using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace Multisponsor.LoyaltyNetwork.WebAPI.Controllers
{
    [RoutePrefix("api/ZonaM")]
    public class ZonaMController : ApiController
    {


        [HttpPost]
        [Route("Get")]

        public async Task<IHttpActionResult> Get(JObject form)
        {
            dbMultisponsorAlliancesEntitiesMore db = new dbMultisponsorAlliancesEntitiesMore();
            try
            {
                var identificationNumber = string.Empty;
                dynamic jsonObject = form;

                try
                {
                    identificationNumber = jsonObject.IdentificationNumber.Value;
                }
                catch
                {
                    return BadRequest("Incorrect call.");
                }

                var user = db.MS_CONTACT
                    .Where(u => u.IDENTIFICATION_NUMBER.ToLower() == identificationNumber.ToLower())
                    .FirstOrDefault();
                if (user == null)
                {
                    return NotFound();
                }



                if (user == null)
                {
                    return NotFound();
                }


                return BadRequest("The password can't be changed.");

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        public static MS_CONTACT GetInfo(int typeidentification, string identification)
        {
            using (var db = new dbMultisponsorAlliancesEntitiesMore())
            {
                var query = (from contact in db.MS_CONTACT
                             join company in db.MS_COMPANY on contact.COMPANY_ID equals company.GUID
                             join program_ally in db.UM_PROGRAM_ALLY on company.GUID equals program_ally.UM_COMPANY_ALLY
                             join program in db.MS_PROGRAM on program_ally.PROGRAM_ID equals program.ID
                             where contact.IDENTIFICATION_TYPE_ID == typeidentification && contact.IDENTIFICATION_NUMBER.ToString() == identification
                             select contact);



                return query.FirstOrDefault();
            }
        }

        [HttpPost]
        [Route("GetUserByIdentification")]
        public Ms_ContactResponse GetUserByIdentification(AccountClientRequest accountClient)
        {


            //ExceptionLogging.LogInfo("ingreso a metodo");
            Ms_ContactResponse response = new Ms_ContactResponse();
            var programId = accountClient.ProgramId;
            var identificationTypeId = accountClient.IdentificationTypeId;
            var identificationNumber = accountClient.IdentificationNumber;

            //ExceptionLogging.LogInfo("programId " + programId);
            //ExceptionLogging.LogInfo("identificationTypeId " + identificationTypeId);
            //ExceptionLogging.LogInfo("identificationNumber " + identificationNumber);
            ///////////////////////////////////////////////////////////////         

            if (string.IsNullOrEmpty(identificationNumber))
            {
                //return BadRequest("Usuario no acto");
                //return response = new AccountClientResponse { Result = false, Code = 100, Message = "Se debe ingresar un número de identificacion." };
            }

            if (string.IsNullOrEmpty(Convert.ToString(identificationTypeId)))
            {
                //return BadRequest("Usuario no acto");
                //return response = new AccountClientResponse { Result = false, Code = 101, Message = "Se debe ingresar un tipo de identificacion." };
            }
            //ExceptionLogging.LogInfo("paso 1");
            ////////////////////////////////////////////


            DataContext.MS_CONTACT consumer = new DataContext.MS_CONTACT();
            //DataContext.Account account = new Core.Domain.Account();



            try
            {
                consumer=GetInfo(identificationTypeId, identificationNumber);

                if (consumer == null)
                {
                    return response = new Ms_ContactResponse { Result = false, Code = 100, Message = "Correcto" };
                }
                else
                {
                    return response = new Ms_ContactResponse { Result = false, Code = 100, Message = "No" };
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }




            return response;

        }
    }
}
